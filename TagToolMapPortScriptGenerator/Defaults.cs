﻿using System;

namespace TagToolMapPortScriptGenerator
{
    class Defaults
    {
        public static string Halo3MapsPath = @"D:\Games\Consoles\X360\Halo_3\maps";
        public static string ODSTMapsPath = @"D:\Games\Consoles\X360\Halo_3_ODST\maps";
        public static string AudioCachePath = @"C:\test\AudioCache";
        public static string CommandsOutputPath = @"C:\Games\test";
        public static string PortTagParams = "";

        public static bool Halo3MP = false, Halo3C = false, Halo3ODST = false, Halo3Hud = false, Halo3Weapons = false, AIO = false;
        public static bool AudioCache = false, Halo3Maps = false, ODSTMaps = false, CommandsOutput = false;
        public static bool CloseOnExit = false, HasCLI = false, ExtraParams = false, ReplaceParams = false;

        // Things added to TagTool
        public static bool PrematchCameraPoints = false;

        public static void Set(string[] args)
        {
            string replaceParams = null, extraParams = null;
            if (args.Length > 0)
            {
                if (args.Length > 1 && args[0].ToLower() != "closeonexit")
                    HasCLI = true;

                foreach (var arg in args)
                {
                    switch (arg.ToLower())
                    {
                        case "closeonexit":
                            CloseOnExit = true;
                            break;
                        case "halo3mp":
                            Halo3MP = true;
                            break;
                        case "halo3c":
                            Halo3C = true;
                            break;
                        case "halo3odst":
                            Halo3ODST = true;
                            break;
                        case "halo3hud":
                            Halo3Hud = true;
                            break;
                        case "halo3weapons":
                            Halo3Weapons = true;
                            break;
                        case "allinone":
                            AIO = true;
                            break;
                        default:
                            if (arg.Contains("="))
                            {
                                var argrs = arg.Split('=');
                                switch (argrs[0].ToLower())
                                {
                                    case "audiocache":
                                        AudioCachePath = argrs[1];
                                        AudioCache = true;
                                        break;
                                    case "halo3maps":
                                        Halo3MapsPath = argrs[1];
                                        Halo3Maps = true;
                                        break;
                                    case "odstmaps":
                                        ODSTMapsPath = argrs[1];
                                        ODSTMaps = true;
                                        break;
                                    case "output":
                                        CommandsOutputPath = argrs[1];
                                        CommandsOutput = true;
                                        break;
                                    case "extraparams":
                                        extraParams = argrs[1];
                                        ExtraParams = true;
                                        break;
                                    case "replaceparams":
                                        replaceParams = argrs[1];
                                        ReplaceParams = true;
                                        break;
                                }
                            }
                            break;
                    }
                }
            }

            using (Helpers h = new Helpers())
            {
                if (!HasCLI)
                {
                    h.PortTagReplaceParams(ref PortTagParams);
                    h.PortTagExtraParams(ref PortTagParams);
                    h.GenerateCheck("Halo3MP", out Halo3MP);
                    h.GenerateCheck("Halo3C", out Halo3C);
                    h.GenerateCheck("Halo3ODST", out Halo3ODST);
                    h.GenerateCheck("Halo3Hud", out Halo3Hud);
                    h.GenerateCheck("Halo3Weapons", out Halo3Weapons);
                    h.GenerateCheck("All in one", out AIO);
                }

                if (ExtraParams && !ReplaceParams)
                    PortTagParams = $"{PortTagParams} {extraParams}";
                if (ReplaceParams && !ExtraParams)
                    PortTagParams = replaceParams;
                if (ReplaceParams && ExtraParams)
                    PortTagParams = $"{replaceParams} {extraParams}";


                if (!AudioCache)
                    h.CheckInput("audio cache", ref AudioCachePath);

                if (!Halo3Maps)
                    if (Halo3MP || Halo3C || Halo3Hud || Halo3Weapons)
                        h.CheckInput("Halo 3 maps", ref Halo3MapsPath);
                if (!ODSTMaps)
                    if (Halo3ODST)
                        h.CheckInput("ODST maps", ref ODSTMapsPath);

                if (!CommandsOutput)
                    h.CheckInput(".cmds output", ref CommandsOutputPath);

                Console.Clear();
            }
        }
    }
}
