﻿using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    public class Fixes
    {
        public class Commands
        {
            public static List<List<string>> Single()
            {
                List<List<string>> commands = new List<List<string>>();
                TagFixes.ForEach(fix =>
                {
                    var temp = new List<string> { $"Echo {fix.Desc}" };
                    temp.Add($"EditTag {fix.TagName}");
                    fix.Commands.ForEach(temp.Add);
                    temp.Add("SaveTagChanges");
                    temp.Add("ExitTo tags");
                    commands.Add(temp);
                });
                return commands;
            }
            public static List<List<string>> Mass()
            {
                List<List<string>> commands = new List<List<string>>();
                MassTagFixes.ForEach(massfix =>
                {
                    var temp = new List<string> { $"Echo {massfix.Desc}" };
                    massfix.TagNames.ForEach(tagname => {
                        var temp2 = new List<string> { $"EditTag {tagname}" };
                        massfix.Commands.ForEach(temp2.Add);
                        temp2.Add("SaveTagChanges");
                        temp2.Add("ExitTo tags");
                        temp.AddRange(temp2);
                    });
                    commands.Add(temp);
                });
                return commands;
            }
            static readonly List<Fix> TagFixes = new List<Fix>()
            {
                new Fix {
                    Desc = "Hack fix: midship crashing around the top platform",
                    TagName = @"levels\dlc\midship\midship.sbsp",
                    Commands = new List<string> {
                        "SetField Materials[090].RenderMethod 0x101F"
                    }
                },
                new Fix {
                    Desc = "Hack fix: trees shaders on Avalanche",
                    TagName = @"levels\dlc\sidewinder\shaders\side_tree_branch_snow.rmsh",
                    Commands = new List<string> {
                        "SetField ShaderProperties[0].Unknown7 0"
                    }
                },
                new Fix {
                    Desc = "Hack fix: remove broken shaders that crash snowbound",
                    TagName = @"levels\multi\snowbound\snowbound.sbsp",
                    Commands = new List<string> {
                        "SetField Materials[41].RenderMethod 0x101F",
                        "SetField Materials[42].RenderMethod 0x101F"
                    }
                },
                new Fix {
                    Desc = "Hack fix: Construct and Citadel shaders (from V2.1)",
                    TagName = @"levels\multi\construct\shaders\waste_panel_pipe.shader",
                    Commands = new List<string> {
                        "SetArgument env_tint_color 0 0 0 1",
                        "SetArgument normal_specular_tint 0.05 0.05 0.05 0",
                        "SetArgument self_illum_intensity 0.005 0.005 0.005 0"
                    }
                },
                new Fix {
                    Desc = "Hack fix: Construct and Citadel shaders (from V2.1)",
                    TagName = @"levels\solo\100_citadel\shaders\panel_wall_alcove.shader",
                    Commands = new List<string> {
                        @"SetField ShaderProperties[0].shaderMaps[0].bitmap levels\dlc\fortress\bitmaps\panel_wall_alcove_diffuse.bitmap",
                        @"SetField shaderproperties[0].shadermaps[2].bitmap levels\solo\100_citadel\bitmaps\panel_wall_alcove_bump.bitmap"
                    }
                },
                // Added to TagTool
                //new Fix {
                //    Desc = "Hack fix: Snowbound sky (A)",
                //    TagName = @"levels\multi\snowbound\sky\sky.mode",
                //    Commands = new List<string> {
                //        @"SetField Materials[11].RenderMethod levels\multi\snowbound\sky\shaders\dust_clouds.rmsh"
                //    }
                //},
                //new Fix {
                //    Desc = "Hack fix: Snowbound sky (B)",
                //    TagName = @"levels\multi\snowbound\sky\shaders\skydome.rmsh",
                //    Commands = new List<string> {
                //        @"SetField ShaderProperties[0].Template shaders\shader_templates\_0_0_0_0_0_0_0_0_0_0_0_0.rmt2"
                //    }
                //},
                new Fix {
                    Desc = "Hack fix: Snowbound base metal (A)",
                    TagName = @"levels\multi\snowbound\shaders\cov_metalplates_icy.rmsh",
                    Commands = new List<string> {
                        @"SetField ShaderProperties[0].ShaderMaps[0].Bitmap levels\multi\snowbound\bitmaps\metal\fs_panel_f_purple_dif.bitmap"
                    }
                },
                new Fix {
                    Desc = "Hack fix: Snowbound base metal (B)",
                    TagName = @"levels\multi\snowbound\shaders\cov_glass_window_opaque.rmsh",
                    Commands = new List<string> {
                        @"SetField ShaderProperties[0].Template shaders\shader_templates\_0_2_0_1_7_0_0_0_0_1_0.rmt2",
                        @"EditBlock ShaderProperties[0]",
                        @"SetArgument analytical_specular_contribution 100 100 100 100",
                        @"SetArgument area_specular_contribution 0.1 0.1 0.1 0.1",
                        @"SetArgument bump_detail_coefficient 5 5 5 5",
                        @"SetArgument roughness 0.2 0.2 0.2 0.2",
                        @"SetArgument specular_coefficient 0.2 0.2 0.2 0.2",
                        @"SetArgument specular_tint 0.5 0.3 0.1 1"
                    }
                }
            };

            static readonly List<MassFix> MassTagFixes = new List<MassFix>()
            {
                new MassFix
                {
                    Desc = "Hack fix: Orbital's steam",
                    Commands = new List<string> {
                        @"SetField RenderMethod.ShaderProperties[0].Template shaders\particle_templates\_4_10_0_0_1_1_1_0_0_0.rmt2"
                    },
                    TagNames = new List<string> {
                        @"levels\dlc\spacecamp\fx\brainiac_mist\brainiac_mist.prt3",
                        @"levels\dlc\spacecamp\fx\pit_steam\pit_steam.prt3"
                    }
                },
                new MassFix
                {
                    Desc = "Hack fix: Cold Storage flood_danglers null PoweredChainsBlock",
                    Commands = new List<string> {
                        "RemoveBlockElements PoweredChains 0 *"
                    },
                    TagNames = new List<string> {
                        @"objects\levels\solo\060_floodship\flood_danglers\large_dangler\large_dangler.physics_model",
                        @"objects\levels\solo\060_floodship\flood_danglers\small_dangler\small_dangler.physics_model"
                    }
                },
                new MassFix
                {
                    Desc = "Hack fix: add a missing weird tagblock for sandbox's grid shaders",
                    Commands = new List<string> {
                        "EditBlock ShaderProperties[0]",
                        "RemoveBlockElements Unknown 0 *",
                        "AddBlockElements Unknown"
                    },
                    TagNames = new List<string> {
                        @"objects\levels\dlc\sandbox\shaders\sandbox_grid_dim.rmhg",
                        @"objects\levels\dlc\sandbox\shaders\sandbox_grid.rmhg"
                    }
                },
                new MassFix
                {
                    Desc = "Hack fix: Construct and Citadel shaders (from V2.1)",
                    Commands = new List<string> {
                        "SetArgument env_tint_color 0 0 0 1"
                    },
                    TagNames = new List<string> {
                        @"levels\solo\070_waste\shaders\panel_080_floor_ceiling.shader",
                        @"levels\solo\070_waste\shaders\panel_080_wall_side.shader",
                        @"levels\solo\070_waste\shaders\panel_catwalk.shader",
                        @"levels\solo\070_waste\shaders\panel_floor_waste.shader",
                        @"levels\solo\070_waste\shaders\panel_poop_railing.shader",
                        @"levels\solo\070_waste\shaders\panel_waste_poop.shader",
                        @"levels\solo\070_waste\shaders\waste_door_frame_small.shader",
                        @"levels\solo\070_waste\shaders\waste_metal_a.shader",
                        @"levels\solo\070_waste\shaders\waste_metal_b.shader",
                        @"levels\solo\070_waste\shaders\waste_metal_silver.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_column.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_floor.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_pipe_blue.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_trim.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_trim_blue.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_wall_diagonal.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_wall_horizontal.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_wall_inset.shader",
                        @"levels\solo\070_waste\shaders\waste_panel_wall_inset_decor_blue.shader",

                        @"levels\solo\100_citadel\shaders\glass_activation.shader",
                        @"levels\solo\100_citadel\shaders\glass_control.shader",
                        @"levels\solo\100_citadel\shaders\panel_bsp_160_floor.shader",
                        @"levels\solo\100_citadel\shaders\panel_ceiling_corner.shader",
                        @"levels\solo\100_citadel\shaders\panel_column_activation.shader",
                        @"levels\solo\100_citadel\shaders\panel_elevator_generator.shader",
                        @"levels\solo\100_citadel\shaders\panel_floor_activation.shader",
                        @"levels\solo\100_citadel\shaders\panel_floor_ele_corner.shader",
                        @"levels\solo\100_citadel\shaders\panel_glass.shader",
                        @"levels\solo\100_citadel\shaders\panel_hall_doorway.shader",
                        @"levels\solo\100_citadel\shaders\panel_hall_floor.shader",
                        @"levels\solo\100_citadel\shaders\panel_light_ceiling.shader",
                        @"levels\solo\100_citadel\shaders\panel_poop_ramp.shader",
                        @"levels\solo\100_citadel\shaders\panel_reactor_base.shader",
                        @"levels\solo\100_citadel\shaders\panel_wall_alcove.shader",
                        @"levels\solo\100_citadel\shaders\panel_wall_control_floor_2.shader",
                        @"levels\solo\100_citadel\shaders\panel_wall_vert.shader",

                        @"levels\multi\construct\shaders\con_panel_wall.shader",
                        @"levels\multi\construct\shaders\const_panel_trim_illum.shader",
                        @"levels\multi\construct\shaders\deck_floor.shader",
                        @"levels\multi\construct\shaders\deck_floor2.shader",
                        @"levels\multi\construct\shaders\glass_activation.shader",
                        @"levels\multi\construct\shaders\glass_floor2.shader",
                        @"levels\multi\construct\shaders\panel_080_column.shader",
                        @"levels\multi\construct\shaders\panel_080_floor.shader",
                        @"levels\multi\construct\shaders\panel_080_wall_back02.shader",
                        @"levels\multi\construct\shaders\panel_080_wall_side_silver.shader",
                        @"levels\multi\construct\shaders\panel_catwalk.shader",
                        @"levels\multi\construct\shaders\panel_cust_arch_int.shader",
                        @"levels\multi\construct\shaders\panel_deco_trim.shader",
                        @"levels\multi\construct\shaders\panel_door_trim.shader",
                        @"levels\multi\construct\shaders\panel_glass.shader",
                        @"levels\multi\construct\shaders\panel_glass2.shader",
                        @"levels\multi\construct\shaders\panel_hall_floor.shader",
                        @"levels\multi\construct\shaders\panel_pipe_simple2.shader",
                        @"levels\multi\construct\shaders\panel_plate_ext.shader",
                        @"levels\multi\construct\shaders\panel_poop_emitter_core.shader",
                        @"levels\multi\construct\shaders\panel_poop_ramp_tech.shader",
                        @"levels\multi\construct\shaders\panel_poop_vent.shader",
                        @"levels\multi\construct\shaders\panel_waste_poop.shader",
                        @"levels\multi\construct\shaders\panel_wedge.shader",
                        @"levels\multi\construct\shaders\riverworld_metalplate.shader",
                        @"levels\multi\construct\shaders\waste_panel_reactor.shader",

                        @"objects\levels\multi\salvation\shaders\altar_holo_projector.shader",
                        @"levels\multi\salvation\shaders\light_circle_lift.shader",
                        @"levels\multi\salvation\shaders\light_circle_lift_unlit.shader",
                        @"levels\multi\salvation\shaders\panel_cust_arch_int.shader",
                        @"levels\multi\salvation\shaders\panel_deco_trim.shader",
                        @"levels\multi\salvation\shaders\panel_deco_trim_ext.shader",
                        @"levels\multi\salvation\shaders\panel_door_trim.shader",
                        @"levels\multi\salvation\shaders\panel_door_trim_ext.shader",
                        @"levels\multi\salvation\shaders\panel_door_trim_illum.shader",
                        @"levels\multi\salvation\shaders\panel_plate.shader",
                        @"levels\multi\salvation\shaders\panel_plate_ext.shader",
                        @"levels\multi\salvation\shaders\panel_simple.shader",
                        @"levels\multi\salvation\shaders\panel_simple_ext.shader",
                        @"levels\multi\salvation\shaders\panel_tech_illum_warm.shader",
                        @"levels\multi\salvation\shaders\panel_tech_railing.shader",
                        @"levels\multi\salvation\shaders\panel_tech_trim.shader",
                        @"levels\multi\salvation\shaders\panel_tech_trim_ext.shader",
                        @"levels\multi\salvation\shaders\panel_wedge.shader",
                        @"levels\multi\salvation\shaders\panel_wedge_unlit.shader",
                        @"levels\multi\salvation\shaders\sal_floor_circle.shader",
                        @"levels\multi\salvation\shaders\sal_floor_hallway.shader",
                        @"levels\multi\salvation\shaders\sal_floor_tile.shader",
                        @"levels\multi\salvation\shaders\sal_floor_tile_ext.shader",
                        @"levels\multi\salvation\shaders\sal_glass_floor.shader",
                        @"levels\multi\salvation\shaders\sal_glass_white.shader",
                        @"levels\multi\salvation\shaders\sal_glass_white_prepass.shader",
                        @"levels\multi\salvation\shaders\salvation_terrain.shader_terrain",
                        @"levels\multi\salvation\shaders\salvation_terrain_ext.shader_terrain",
                        @"levels\multi\salvation\shaders\trim_trim.shader",
                        @"levels\multi\salvation\shaders\trim_trim_sm.shader",

                        @"levels\dlc\fortress\shaders\floor_glass.shader",
                        @"levels\dlc\fortress\shaders\glass_panel_wall_inset.shader",
                        @"levels\dlc\fortress\shaders\light_basic_blue.shader",
                        @"levels\dlc\fortress\shaders\light_basic_red.shader",
                        @"levels\dlc\fortress\shaders\panel_hall_floor.shader",
                        @"levels\dlc\fortress\shaders\panel_pipe_blue.shader",
                        @"levels\dlc\fortress\shaders\panel_pipe_red.shader",
                        @"levels\dlc\fortress\shaders\panel_pipe_simple.shader",
                        @"levels\dlc\fortress\shaders\panel_platform_center.shader",
                        @"levels\dlc\fortress\shaders\panel_wall_alcove.shader",
                        @"levels\dlc\fortress\shaders\panel_wall_alcove_illum.shader",
                        @"levels\dlc\fortress\shaders\panel_wall_alcove_metal.shader"
                    }
                }
            };
        }

        struct MassFix
        {
            public string Desc;
            public List<string> TagNames, Commands;
        };
        struct Fix
        {
            public string Desc, TagName;
            public List<string> Commands;
        };
    }
}
