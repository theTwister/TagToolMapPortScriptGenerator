﻿using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    class Hud
    {
        public static List<List<string>> Commands()
        {
            List<List<string>> commands = new List<List<string>> {
                new List<string> {
                    $"OpenCacheFile {Quote($@"{Defaults.Halo3MapsPath}\construct.map")}",
                    $"PortTag Replace *.chgd",
                    $"PortTag Replace *.chdt",
                    "Exit"
                }
            };
            return commands;
        }
        public static string Quote(string input) => $"\"{input}\"";
    }
}
