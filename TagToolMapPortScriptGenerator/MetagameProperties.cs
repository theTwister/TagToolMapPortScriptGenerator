﻿using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    class MetagameProperties
    {
        public static List<List<string>> Commands()
        {
            List<List<string>> commands = new List<List<string>> {
                new List<string> {
                $"Echo Removing MetagameProperties from all char tags",
                $"ForEach char RemoveBlockElements MetagameProperties 0 *"
            }};
            return commands;
        }

        // Depricated: Using ForEach char instead
        public static readonly List<string> CharacterTagNames = new List<string> {
            @"ai\generic",
            @"ai\generic_turret",

            @"objects\characters\brute\ai\brute",
            @"objects\characters\brute\ai\brute_bodyguard",
            @"objects\characters\brute\ai\brute_bodyguard_no_grenade",
            @"objects\characters\brute\ai\brute_captain",
            @"objects\characters\brute\ai\brute_captain_major",
            @"objects\characters\brute\ai\brute_captain_major_no_grenade",
            @"objects\characters\brute\ai\brute_captain_no_grenade",
            @"objects\characters\brute\ai\brute_captain_ultra",
            @"objects\characters\brute\ai\brute_chieftain_armor",
            @"objects\characters\brute\ai\brute_chieftain_armor_no_grenade",
            @"objects\characters\brute\ai\brute_chieftain_weapon",
            @"objects\characters\brute\ai\brute_jumppack",
            @"objects\characters\brute\ai\brute_major",
            @"objects\characters\brute\ai\brute_phantom",
            @"objects\characters\brute\ai\brute_stalker",
            @"objects\characters\brute\ai\brute_ultra",

            @"objects\characters\bugger\ai\bugger",
            @"objects\characters\bugger\ai\bugger_major",

            @"objects\characters\civilians\worker\ai\worker",
            @"objects\characters\civilians\worker\ai\worker_wounded",

            @"objects\characters\cortana\ai\cortana",

            @"objects\characters\dervish\ai\dervish",

            @"objects\characters\elite\ai\elite",
            @"objects\characters\elite\ai\elite_major",
            @"objects\characters\elite\ai\elite_specops",
            @"objects\characters\elite\ai\elite_specops_commander",

            @"objects\characters\floodcarrier\ai\flood_carrier",
            @"objects\characters\floodcombat_base\ai\floodcombat_base",
            @"objects\characters\floodcombat_brute\ai\floodcombat_brute",
            @"objects\characters\floodcombat_elite\ai\floodcombat_elite",
            @"objects\characters\floodcombat_elite\ai\floodcombat_elite_shielded",
            @"objects\characters\floodcombat_human\ai\flood_combat_human",

            @"objects\characters\flood_infection\ai\flood_infection",
            @"objects\characters\flood_pureforms\ai\flood_pureform",
            @"objects\characters\flood_ranged\ai\flood_pureform_ranged",
            @"objects\characters\flood_stalker\ai\flood_pureform_stalker",
            @"objects\characters\flood_tank\ai\flood_pureform_tank",

            @"objects\characters\grunt\ai\grunt",
            @"objects\characters\grunt\ai\grunt_heavy",
            @"objects\characters\grunt\ai\grunt_major",
            @"objects\characters\grunt\ai\grunt_ultra",

            @"objects\characters\hunter\ai\hunter",

            @"objects\characters\jackal\ai\jackal",
            @"objects\characters\jackal\ai\jackal_major",
            @"objects\characters\jackal\ai\jackal_sniper",

            @"objects\characters\marine\ai\marine",
            @"objects\characters\marine\ai\marine_female",
            @"objects\characters\marine\ai\marine_johnson",
            @"objects\characters\marine\ai\marine_johnson_boss",
            @"objects\characters\marine\ai\marine_johnson_halo",
            @"objects\characters\marine\ai\marine_no_trade_weapon",
            @"objects\characters\marine\ai\marine_odst",
            @"objects\characters\marine\ai\marine_odst_sgt",
            @"objects\characters\marine\ai\marine_pilot",
            @"objects\characters\marine\ai\marine_sgt",
            @"objects\characters\marine\ai\marine_wounded",
            @"objects\characters\marine\ai\naval_officer",

            @"objects\characters\miranda\ai\miranda",

            @"objects\characters\monitor\ai\monitor",
            @"objects\characters\monitor\ai\monitor_combat",

            @"objects\characters\sentinel_aggressor\ai\sentinel_aggressor",
            @"objects\characters\sentinel_aggressor\ai\sentinel_aggressor_captain",
            @"objects\characters\sentinel_aggressor\ai\sentinel_aggressor_major",
            @"objects\characters\sentinel_constructor\ai\sentinel_constructor",

            @"objects\characters\truth\ai\truth",

            @"objects\equipment\autoturret\autoturret",

            @"objects\giants\scarab\ai\scarab"
        };
    }
}
