﻿using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    class ForgePalette
    {
        public static List<List<string>> MakePortCommands(GameVersion gameVersion)
        {
            Map.GetInfos(gameVersion, out List<MapInfo> infos, out string Version, out string ScenarioPath);
            List<List<string>> commands = new List<List<string>>();
            var temp = new List<string> { @"EditTag levels\multi\guardian\guardian.scnr" };
            temp.Add($"Echo Updating the forge palette ({Version})");
            infos.ForEach(info => {
                temp.Add($@"CopyForgePalette {ScenarioPath}\{info.FileName}\{info.FileName}.scnr");
            });
            temp.Add("SaveTagChanges");
            temp.Add("ExitTo tags");
            commands.Add(temp);
            return commands;
        }
    }
}
