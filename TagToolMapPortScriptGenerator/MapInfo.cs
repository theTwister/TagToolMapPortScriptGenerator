﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TagToolMapPortScriptGenerator
{
    class Map
    {
        public static bool MainMenuCache = true;
        public static List<List<string>> MakePortCommands(GameVersion gameVersion, string MapsPath)
        {
            GetInfos(gameVersion, out List<MapInfo> infos, out string Version, out string ScenarioPath);
            List<List<string>> commands = new List<List<string>>();
            var temp = new List<string>();
            infos.ForEach(info => {
                temp.Add($"Echo Porting {info.MenuName} ({Version})");
                temp.Add($"OpenCacheFile {Quote($@"{MapsPath}\{info.FileName}.map")}");
                temp.Add($@"PortTag {Defaults.PortTagParams} {ScenarioPath}\{info.FileName}\{info.FileName}.scnr");
                temp.Add("Exit");
            });
            temp.Add($"UpdateMapFiles {Quote($@"{MapsPath}\info\")}");
            commands.Add(temp);
            return commands;
        }

        public static void GetInfos(GameVersion gameVersion, out List<MapInfo> mapInfos, out string version, out string scenarioPath)
        {
            List<MapInfo> MapInfoList = new List<MapInfo>();
            string versionText = null;
            string scenarioText = null;
            Infos.ForEach(Info => {

                if (Info.Item1 == gameVersion)
                {
                    switch (gameVersion)
                    {
                        case GameVersion.Halo3Multiplayer:
                            if (MainMenuCache)
                                MapInfoList = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }.Select(i => Info.Item2[i]).ToList();
                            else
                                MapInfoList = new List<int> { 1, 5, 7, 9 }.Select(i => Info.Item2[i]).ToList();

                            versionText = "Halo 3 Multiplayer";
                            scenarioText = @"levels\multi";
                            break;
                        case GameVersion.Halo3MultiplayerDLC:
                            if (MainMenuCache)
                                MapInfoList = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }.Select(i => Info.Item2[i]).ToList();
                            else
                                MapInfoList = new List<int> { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }.Select(i => Info.Item2[i]).ToList();

                            versionText = "Halo 3 Multiplayer DLC";
                            scenarioText = @"levels\dlc";
                            break;
                        case GameVersion.Halo3Campaign:
                            MapInfoList = new List<int> { 1, 0, 2, 3, 4, 6, 7, 8, 9, 10, 5 }.Select(i => Info.Item2[i]).ToList();
                            versionText = "Halo 3 Campaign";
                            scenarioText = @"levels\solo";
                            break;
                        case GameVersion.Halo3ODSTCampaign:
                            versionText = "Halo 3: ODST Campaign";
                            scenarioText = @"levels\atlas";
                            break;
                    }
                }
            });
            mapInfos = MapInfoList;
            version = versionText;
            scenarioPath = scenarioText;
        }

        public static readonly List<Tuple<GameVersion, List<MapInfo>>> Infos = new List<Tuple<GameVersion, List<MapInfo>>>
        {
            ToMapInfo(GameVersion.Halo3Multiplayer, new List<MapInfo> // Halo 3 Retail Maps
            {
                new MapInfo { FileName = "chill",           MenuName = "Narrows",       Position = "0.1023 13.2014 67.2402",    Orientation = "-91.8674 0.5628 16.7653"     }, // 0
                new MapInfo { FileName = "construct",       MenuName = "Construct",     Position = "4.5471 1.8711 13.4354",     Orientation = "-126.7653 0 0"               }, // 1
                new MapInfo { FileName = "cyberdyne",       MenuName = "The Pit",       Position = "16.4839 -0.2954 5.9263",    Orientation = "148.4995 10.9499 -6.6396"    }, // 2
                new MapInfo { FileName = "deadlock",        MenuName = "Highground",    Position = "-7.9039, -4.0816, 17.2834", Orientation = "74.7631 -4.6530 -16.5844"    }, // 3
                new MapInfo { FileName = "guardian",        MenuName = "Guardian",      Position = "0 0 0",                     Orientation = "-58.089 -6.8396 10.8268"     }, // 4
                new MapInfo { FileName = "isolation",       MenuName = "Isolation",     Position = "-14.4359 -10.9502 -5.2309", Orientation = "30 0 0"                      }, // 5
                new MapInfo { FileName = "riverworld",      MenuName = "Valhallla",     Position = "80, -115, 8",               Orientation = "-72 0 0"                     }, // 6
                new MapInfo { FileName = "salvation",       MenuName = "Epitaph",       Position = "-0.0762 -0.1681 7.1527",    Orientation = "90 0 0"                      }, // 7
                new MapInfo { FileName = "shrine",          MenuName = "Sandtrap",      Position = "31.1949, 20.94, -6.8599",   Orientation = "-137.8311 16.6954 15.1673"   }, // 8
                new MapInfo { FileName = "snowbound",       MenuName = "Snowbound",     Position = "-7.1015 17.7492 3.9918",    Orientation = "-90 0 0"                     }, // 9
                new MapInfo { FileName = "zanzibar",        MenuName = "Last Resort",   Position = "7.11076 30.8905 -1.48192",  Orientation = "-125.6226 -2.24079 -3.12343" }  // 10
            }),
            ToMapInfo(GameVersion.Halo3MultiplayerDLC, new List<MapInfo> // Halo 3 DLC Maps
            {
                new MapInfo { FileName = "armory",          MenuName = "Rat's Nest",    Position = "-16.8807 0.0363 -8.6872",   Orientation = "0 0 0"                       }, // 0
                new MapInfo { FileName = "bunkerworld",     MenuName = "Standoff",      Position = "1.9198 39.4172 14.7578",    Orientation = "0 0 0"                       }, // 1
                new MapInfo { FileName = "chillout",        MenuName = "Cold Storage",  Position = "0.7120 -10.8107 5.3540",    Orientation = "90 0 0"                      }, // 2
                new MapInfo { FileName = "descent",         MenuName = "Assembly",      Position = "-19.9727 -0.0140 -17.3611", Orientation = "0 0 0"                       }, // 3
                new MapInfo { FileName = "docks",           MenuName = "Longshore",     Position = "-28.5603 22.1670 -3.9043",  Orientation = "0 0 0"                       }, // 4
                new MapInfo { FileName = "fortress",        MenuName = "Citadel",       Position = "-33.9909 3.4858 -18.9907",  Orientation = "0 0 0"                       }, // 5
                new MapInfo { FileName = "ghosttown",       MenuName = "Ghost town",    Position = "-10.6792 10.8319 5.6487",   Orientation = "0 0 0"                       }, // 6
                new MapInfo { FileName = "lockout",         MenuName = "Blackout",      Position = "-19.9729 0.4024 -5.3355",   Orientation = "0 0 0"                       }, // 7
                new MapInfo { FileName = "midship",         MenuName = "Heretic",       Position = "-5.7814 4.7866 4.5577",     Orientation = "0 0 0"                       }, // 8
                new MapInfo { FileName = "sandbox",         MenuName = "Sandbox",       Position = "-24.9556 -9.8958 -17.2465", Orientation = "0 0 0"                       }, // 9
                new MapInfo { FileName = "sidewinder",      MenuName = "Avalanche",     Position = "-35.8092 42.7776 2.6463",   Orientation = "0 0 0"                       }, // 10
                new MapInfo { FileName = "spacecamp",       MenuName = "Orbital",       Position = "-8.7606 17.2195 -0.3308",   Orientation = "0 0 0"                       }, // 11
                new MapInfo { FileName = "warehouse",       MenuName = "Foundry",       Position = "-11.2818 0.2725 4.1475",    Orientation = "0 0 0"                       }  // 12
            }),
            ToMapInfo(GameVersion.Halo3Campaign, new List<MapInfo> // Halo 3 Campaign Maps
            {
                new MapInfo { FileName = "005_intro",       MenuName = "Arrival" },
                new MapInfo { FileName = "010_jungle",      MenuName = "Sierra 117" },
                new MapInfo { FileName = "020_base",        MenuName = "Crow's Nest" },
                new MapInfo { FileName = "030_outskirts",   MenuName = "Tsavo Highway" },
                new MapInfo { FileName = "040_voi",         MenuName = "The Storm" },
                new MapInfo { FileName = "050_floodvoi",    MenuName = "Floodgate" },
                new MapInfo { FileName = "070_waste",       MenuName = "The Ark" },
                new MapInfo { FileName = "100_citadel",     MenuName = "The Covenant" },
                new MapInfo { FileName = "110_hc",          MenuName = "Cortana" },
                new MapInfo { FileName = "120_halo",        MenuName = "Halo" },
                new MapInfo { FileName = "130_epilogue",    MenuName = "Epilogue" }
            }),
            ToMapInfo(GameVersion.Halo3ODSTCampaign, new List<MapInfo> // Halo 3 ODST Maps
            {
                new MapInfo { FileName = "c100",            MenuName = "Prepare To Drop" },
                new MapInfo { FileName = "c200",            MenuName = "Costal Highway" },
                new MapInfo { FileName = "h100",            MenuName = "Mombasa Streets" },
                new MapInfo { FileName = "l200",            MenuName = "Data Hive" },
                new MapInfo { FileName = "l300",            MenuName = "Costal Highway" },
                new MapInfo { FileName = "sc100",           MenuName = "Tayari Plaza" },
                new MapInfo { FileName = "sc110",           MenuName = "Uplift Reserve" },
                new MapInfo { FileName = "sc120",           MenuName = "Kizingo Boulevard" },
                new MapInfo { FileName = "sc130",           MenuName = "ONI Alpha Site" },
                new MapInfo { FileName = "sc140",           MenuName = "NMPD HQ" },
                new MapInfo { FileName = "sc150",           MenuName = "Kikowani Station" }
            }),
            ToMapInfo(GameVersion.HaloOnline, new List<MapInfo> { // Saber Maps
                new MapInfo { FileName = "s3d_avalanche",   MenuName = "Diamond Back",  Position = "39.6816 52.9674 13.2453",   Orientation = "-101.3976 1.8404 9.0516"     },
                new MapInfo { FileName = "s3d_edge",        MenuName = "Edge",          Position = "-11.2818 0.2725 4.1475",    Orientation = "0 0 0"                       },
                new MapInfo { FileName = "s3d_reactor",     MenuName = "Reactor",       Position = "-11.2818 0.2725 4.1475",    Orientation = "0 0 0"                       },
                new MapInfo { FileName = "s3d_turf",        MenuName = "Ice Box",       Position = "-11.1375 10.6502 3.6808",   Orientation = "-1.1067 -6.0486 0.1166"      }
            })
        };
        public static string Quote(string input) => $"\"{input}\"";

        static Tuple<GameVersion, List<MapInfo>> ToMapInfo(GameVersion gameVersion, List<MapInfo> mapInfos) => new Tuple<GameVersion, List<MapInfo>>(gameVersion, mapInfos);
    }

    struct MapInfo
    {
        public string FileName, MenuName, Position, Orientation;
    };

    enum GameVersion
    {
        Halo3Multiplayer,
        Halo3MultiplayerDLC,
        Halo3Campaign,
        Halo3ODSTCampaign,
        HaloOnline
    }
}
