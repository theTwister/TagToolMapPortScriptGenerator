﻿using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    class PrematchCameraPoints
    {
        public static List<List<string>> MakePortCommands(GameVersion gameVersion)
        {
            Map.GetInfos(gameVersion, out List<MapInfo> infos, out string Version, out string ScenarioPath);
            List<List<string>> commands = new List<List<string>>();
            infos.ForEach(info => {
                commands.Add(new List<string> {
                    $"Echo Setting prematch camera points for {info.MenuName} ({Version})",
                    $@"EditTag {ScenarioPath}\{info.FileName}\{info.FileName}.scnr",
                    $"SetField CutsceneCameraPoints[0].Position {info.Position}",
                    $"SetField CutsceneCameraPoints[0].Orientation {info.Orientation}",
                    "SaveTagChanges",
                    "ExitTo tags"
                });
            });
            return commands;
        }
    }
}
