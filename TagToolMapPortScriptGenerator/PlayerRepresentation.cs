﻿using System;
using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    class PlayerRepresentation
    {
        public static List<List<string>> MakePortCommands(GameVersion gameVersion, string MapsPath)
        {
            Map.GetInfos(gameVersion, out List<MapInfo> infos, out string Version, out string ScenarioPath);
            List<List<string>> commands = new List<List<string>>();
            if (gameVersion == GameVersion.Halo3ODSTCampaign)
            {
                var temp = new List<string>
                {
                    $"Echo Porting ODST PlayerRepresentation",
                    $"OpenCacheFile {Map.Quote($@"{MapsPath}\{infos[0].FileName}.map")}",
                    @"PortTag Recursive objects\characters\odst_recon\fp\fp.render_model",
                    @"PortTag Recursive objects\characters\odst_recon\fp_body\fp_body.render_model",
                    @"PortTag Recursive objects\characters\odst_oni_op\fp\fp.render_model",
                    @"PortTag Recursive objects\characters\odst_oni_op\body\body.render_model",
                    @"SaveTagNames",
                    @"ExitTo Tags",

                    @"EditTag globals\globals.globals",
                    @"EditBlock PlayerRepresentation 2",
                    @"SetField FirstPersonHands objects\characters\odst_recon\fp\fp.render_model",
                    @"SetField FirstPersonBody objects\characters\odst_recon\fp_body\fp_body.render_model",
                    @"SetField ThirdPersonUnit objects\characters\odst_recon\odst_recon.biped",
                    "SaveTagChanges",
                    "Exit"
                };
                commands.Add(temp);
            }
            return commands;
        }
    }
}
