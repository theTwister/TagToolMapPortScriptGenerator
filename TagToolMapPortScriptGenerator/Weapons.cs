﻿using System.Collections.Generic;

namespace TagToolMapPortScriptGenerator
{
    class Weapons
    {
        public static List<List<string>> Commands()
        {
            List<List<string>> commands = new List<List<string>>();
            var temp = new List<string> { $"OpenCacheFile {Quote($@"{Defaults.Halo3MapsPath}\sandbox.map")}" };
            WeaponsList.ForEach(weapon =>
            {
                weapon.PerWeap.ForEach(per =>
                {
                    var wType = WeaponTypeName[(int)weapon.Type];
                    var wName = per.Name;
                    var wProj = per.ProjectileName;
                    per.Models?.ForEach(type =>
                    {
                        switch (type)
                        {
                            case Model.HasOwn:
                                if (weapon.Type == WeaponType.Grenade)
                                    temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}_grenade\{wName}_grenade.render_model");
                                else
                                    temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\{wName}.render_model");
                                break;
                            case Model.HasFp:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fp_{wName}\fp_{wName}.render_model");
                                break;
                            case Model.HasLod:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\lod\lod_{wName}\lod_{wName}.render_model");
                                break;
                            case Model.HasSpecial:
                                per.MoreModels?.ForEach(model => temp.Add($"PortTag Replace {model}.render_model"));
                                break;
                        };
                    });
                    per.Projectiles?.ForEach(type =>
                    {
                        switch (type)
                        {
                            case Projectile.HasOwn:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\projectiles\{wProj}\{wProj}.render_model");
                                break;
                            case Projectile.HasSpecial:
                                per.MoreProjectiles?.ForEach(projectile => temp.Add($"PortTag Replace MatchShaders {projectile}.render_model"));
                                break;
                        };
                    });
                    per.Animations?.ForEach(animationType =>
                    {
                        switch (animationType)
                        {
                            case Animation.HasOwn:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}_weapon\{wName}_weapon.model_animation_graph");
                                break;
                            case Animation.HasFp:
                                Characters.ForEach(character => temp.Add($@"PortTag Replace MatchShaders objects\characters\{character}\fp\weapons\{wType}\fp_{wName}\fp_{wName}.model_animation_graph"));
                                break;
                            case Animation.HasSpecial:
                                per.MoreAnimations?.ForEach(animation => temp.Add($"PortTag Replace MatchShaders {animation}.model_animation_graph"));
                                break;
                        };
                    });
                    per.Effects?.ForEach(effectType =>
                    {
                        switch (effectType)
                        {
                            case Effect.HasFiring:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\firing.effect");
                                break;
                            case Effect.HasReloading:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\reloading.effect");
                                break;
                            case Effect.HasDetonation:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\detonation.effect");
                                break;
                            case Effect.HasSuperDetonation:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\super_detonation.effect");
                                break;
                            case Effect.HasOverheated:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\overheated.effect");
                                break;
                            case Effect.HasCharging:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\charging.effect");
                                break;
                            case Effect.HasChargedFiring:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\charged\firing.effect");
                                break;
                            case Effect.HasFiringTracer:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\firing_tracer.effect");
                                break;
                            case Effect.HasTracerImpact:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\tracer_impact.effect");
                                break;
                            case Effect.HasProjectile:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\projectile.effect");
                                break;

                            case Effect.HasHammerOn:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\hammer_on.effect");
                                break;
                            case Effect.HasGravityHammerImpact:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\gravity_hammer_impact.effect");
                                break;
                            case Effect.HasFpGravityHammerImpact:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\fp_gravity_hammer_impact.effect");
                                break;

                            case Effect.HasBladeActivate:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\blade_activate.effect");
                                break;
                            case Effect.HasBladeOn:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\blade_on.effect");
                                break;
                            case Effect.HasSwordClash:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\sword_clash.effect");
                                break;
                            case Effect.HasImpact:
                                temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\impact.effect");
                                break;
                            case Effect.HasSpecial:
                                per.MoreEffects?.ForEach(effect => temp.Add($@"PortTag Replace MatchShaders objects\weapons\{wType}\{wName}\fx\{effect}.effect"));
                                break;
                        }
                    });
                    temp.Add("");
                });
                temp.Add("");
            });
            temp.Add("Exit");
            commands.Add(temp);
            return commands;
        }

        static readonly List<string> Characters = new List<string> { "masterchief", "dervish" };

        static readonly List<Weapon> WeaponsList = new List<Weapon>
        {
            new Weapon
            {
                Type = WeaponType.Melee,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per {
                        Name = "energy_blade",
                        Animations = new List<Animation>{ Animation.HasFp, Animation.HasSpecial },
                        Effects = new List<Effect>{ Effect.HasBladeActivate, Effect.HasBladeOn, Effect.HasSwordClash, Effect.HasImpact },
                        MoreAnimations = new List<string>{ @"objects\weapons\melee\energy_blade\fp_energy_blade\cinematics\letterboxes\100ld_confront_truth\100ld_confront_truth" },
                    },
                    new Weapon.Per { Name = "gravity_hammer",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasHammerOn, Effect.HasGravityHammerImpact, Effect.HasFpGravityHammerImpact },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp }
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.Multiplayer,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "assault_bomb",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "ball",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "flag",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.Pistol,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "excavator",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "magnum",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "needler",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasDetonation, Effect.HasSuperDetonation },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                        Projectiles = new List<Projectile>{ Projectile.HasOwn }, ProjectileName = "needler_shard"
                    },
                    new Weapon.Per { Name = "plasma_pistol",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasCharging, Effect.HasOverheated, Effect.HasChargedFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.Rifle,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "assault_rifle",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "battle_rifle",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasProjectile, Effect.HasDetonation },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "beam_rifle",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasProjectile, Effect.HasOverheated },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "covenant_carbine",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasReloading },
                        Models = new List<Model>{ Model.HasOwn, Model.HasFp, Model.HasSpecial }, MoreModels = new List<string>{ @"objects\weapons\lod\lod_carbine\lod_carbine" },
                    },
                    new Weapon.Per { Name = "plasma_rifle",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasOverheated },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "shotgun",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "smg",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "sniper_rifle",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    },
                    new Weapon.Per { Name = "spike_rifle",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.SupportHigh,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "flak_cannon",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasDetonation },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                        Projectiles = new List<Projectile>{ Projectile.HasOwn }, ProjectileName = "flak_bolt"
                    },
                    new Weapon.Per { Name = "rocket_launcher",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasProjectile, Effect.HasDetonation },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                        Projectiles = new List<Projectile>{ Projectile.HasOwn }, ProjectileName = "rocket"
                    },
                    new Weapon.Per { Name = "spartan_laser",
                        Animations = new List<Animation>{ Animation.HasOwn, Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasFiringTracer, Effect.HasCharging, Effect.HasTracerImpact },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.SupportLow,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "brute_shot",
                        Animations = new List<Animation>{ Animation.HasFp },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                        Projectiles = new List<Projectile>{ Projectile.HasOwn }, ProjectileName = "grenade"
                    },
                    new Weapon.Per { Name = "sentinel_gun",
                        Effects = new List<Effect>{ Effect.HasFiring, Effect.HasOverheated },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod, Model.HasFp },
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.Turret,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "flamethrower",
                        Animations = new List<Animation>{ Animation.HasOwn },
                        Effects = new List<Effect>{ Effect.HasFiring },
                        Models = new List<Model>{ Model.HasOwn, Model.HasLod },
                    }
                }
            },
            new Weapon
            {
                Type = WeaponType.Grenade,
                PerWeap = new List<Weapon.Per> {
                    new Weapon.Per { Name = "claymore", Models = new List<Model>{ Model.HasOwn } },
                    new Weapon.Per { Name = "firebomb", Models = new List<Model>{ Model.HasOwn } },
                    new Weapon.Per { Name = "frag", Models = new List<Model>{ Model.HasOwn } },
                    new Weapon.Per { Name = "plasma", Models = new List<Model>{ Model.HasOwn } }
                }
            }
        };

        struct Weapon
        {
            public struct Per
            {
                public string Name, ProjectileName;
                public List<Animation> Animations;
                public List<Effect> Effects;
                public List<Model> Models;
                public List<Projectile> Projectiles;
                public List<string> MoreAnimations, MoreEffects, MoreModels, MoreProjectiles;
            }
            public WeaponType Type;
            public List<Per> PerWeap;
        }

        enum Animation
        {
            HasOwn = 0,
            HasFp,

            HasSpecial,
            Count
        };
        enum Effect
        {
            HasFiring = 0,
            HasReloading,
            HasDetonation,
            HasSuperDetonation,
            HasOverheated,
            HasCharging,
            HasChargedFiring,
            HasFiringTracer,
            HasTracerImpact,
            HasProjectile,
            
            HasHammerOn,
            HasGravityHammerImpact,
            HasFpGravityHammerImpact,

            HasBladeActivate,
            HasBladeOn,
            HasSwordClash,
            HasImpact,

            HasSpecial,
            Count
        };
        enum Model
        {
            HasOwn = 0,
            HasLod,
            HasFp,

            HasSpecial,
            Count
        };
        enum Projectile
        {
            HasOwn = 0,

            HasSpecial,
            Count
        };

        enum WeaponType : int
        {
            Melee = 0,
            Multiplayer,
            Pistol,
            Rifle,
            SupportHigh,
            SupportLow,
            Turret,
            Grenade,

            Count
        };
        public static readonly string[] WeaponTypeName = new string[(int)WeaponType.Count] {
            "melee",
            "multiplayer",
            "pistol",
            "rifle",
            "support_high",
            "support_low",
            "turret",
            "grenade"
        };
        public static string Quote(string input) => $"\"{input}\"";
    }
}

/* Done
characters : masterchief : dervish
types : melee : multiplayer : pistol : rifle : support_high : support_low : turret : grenade
weapons : energy_blade : gravity_hammer
weapons : assault_bomb : ball : flag
weapons : excavator : magnum : needler : plasma_pistol
weapons : assault_rifle : battle_rifle : beam_rifle : covenant_carbine : plasma_rifle : shotgun : smg : sniper_rifle : spike_rifle
weapons : flak_cannon : rocket_launcher : spartan_laser
weapons : brute_shot : sentinel_gun
weapons : flamethrower
weapons : claymore : firebomb : frag : plasma
*/

/* ToDo
types : banshee : hornet
weapon_name : banshee_bomb : plasma_cannon
weapon_name: hornet_missile : chain_gun
objects\vehicles\{type}\fx\weapon\{weapon_name}\firing.effect

objects\vehicles\banshee\fx\weapon\banshee_bomb\firing.effect
objects\vehicles\banshee\fx\weapon\plasma_cannon\firing.effect
objects\vehicles\hornet\fx\weapon\hornet_missile\firing.effect
objects\vehicles\hornet\fx\weapon\chain_gun\firing.effect

objects\weapons\lod\lod_plasma_turret\lod_plasma_turret.render_model

objects\weapons\turret\missile_pod\fx\firing.effect
    
objects\weapons\lod\lod_machine_gun_turret\lod_machine_gun_turret.render_model
objects\weapons\turret\machinegun_turret\fx\firing.effect
objects\weapons\turret\machinegun_turret\machinegun_turret_weapon\machinegun_turret_weapon.model_animation_graph
objects\weapons\turret\machinegun_turret\machinegun_turret_vehicle\machinegun_turret_vehicle.model_animation_graph
    
types : turret
sub_types : weapon : vehicle
weapons : plasma_cannon
objects\weapons\{type}\{weapon}\{weapon}_{sub_type}\{weapon}_{sub_type}.render_model
objects\weapons\{type}\{weapon}\{weapon}_{sub_type}\{weapon}_{sub_type}.model_animation_graph

objects\weapons\turret\plasma_cannon\plasma_cannon_weapon\plasma_cannon_weapon.render_model
objects\weapons\turret\plasma_cannon\plasma_cannon_vehicle\plasma_cannon_vehicle.render_model
objects\weapons\turret\plasma_cannon\plasma_cannon_vehicle\plasma_cannon_vehicle.model_animation_graph
objects\weapons\turret\plasma_cannon\plasma_cannon_vehicle\garbage\garbage_tripod_back\garbage_tripod_back.render_model
objects\weapons\turret\plasma_cannon\plasma_cannon_vehicle\garbage\garbage_tripod_front\garbage_tripod_front.render_model
*/
