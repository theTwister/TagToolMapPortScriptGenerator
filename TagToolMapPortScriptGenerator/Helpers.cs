﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TagToolMapPortScriptGenerator
{
    class Helpers : IDisposable
    {
        public List<string> Script = new List<string>();

        public void CheckFile(string cmds)
        {
            if (File.Exists(Quote($@"{Defaults.CommandsOutputPath}\{cmds}.cmds")))
                File.Delete(Quote($@"{Defaults.CommandsOutputPath}\{cmds}.cmds"));
            if (!Directory.Exists(Quote(Defaults.CommandsOutputPath)))
                Directory.CreateDirectory(Defaults.CommandsOutputPath);
        }

        public void CheckInput(string Output, ref string Input)
        {
            Console.Write($"Enter the {Output} path (press enter for default):\nDefaults: {Input}\n> ");
            var temp = Console.ReadLine();
            if (temp != "")
                Input = temp;
            Console.Write($"New Path {Input}\n");
        }

        public void GenerateCheck(string Script, out bool ShouldGenerate)
        {
            bool shouldGenerate = true;
            Console.Write($"Should generate {Script} (press enter for default):\nOptions: Yes, No\n> ");
            var temp = Console.ReadLine();
            if (temp.ToLower().StartsWith("n"))
                shouldGenerate = false;

            ShouldGenerate = shouldGenerate;
            Console.Write($"{(ShouldGenerate ? "yes" : "no")}\n");
        }

        public void PortTagReplaceParams(ref string Input)
        {
            Console.Write($"Enter the replacement params (press enter for default):\nDefaults: {Input}\n> ");
            var temp = Console.ReadLine();
            if (temp != "")
                Input = temp;
            Console.Write($"New params {Input}\n");
        }

        public void PortTagExtraParams(ref string Input)
        {
            Console.Write($"Enter the extra params (press enter for default):\nDefaults: {Input}\n> ");
            var temp = Console.ReadLine();
            if (temp != "")
                Input += " " + temp;
            Console.Write($"New params {Input}\n");
        }
        public static string Quote(string input) => $"\"{input}\"";

        public void ScriptWriter(List<List<string>> Module, bool ShouldAdd = true) => Module.ForEach(commands => {
            if (ShouldAdd)
            {
                commands.ForEach(Script.Add);
                Script.Add("");
            }
        });

        #region IDisposable Support
        public bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Script.ForEach(Console.WriteLine);
                    Script.Clear();
                }
                disposedValue = true;
            }
        }
        public void Dispose() => Dispose(true);
        #endregion
    }
}
