﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TagToolMapPortScriptGenerator
{
    class Program
    {
        static readonly bool TestAll = true; // This will do everything and is used for easier testing 

        static void Main(string[] args)
        {
            Defaults.PrematchCameraPoints = TestAll;

            if (!TestAll)
                Defaults.Set(args);

            if (TestAll || Defaults.Halo3MP)
                WriteScript(Scripts.Halo3Multiplayer);

            //if (TestAll || Defaults.Halo3C)
            //    WriteScript(Scripts.Halo3Campaign);

            //if (TestAll || Defaults.Halo3ODST)
            //    WriteScript(Scripts.Halo3ODSTCampaign);

            //if (TestAll || Defaults.Halo3Hud)
            //    WriteScript(Scripts.Halo3Hud);

            //if (TestAll || Defaults.Halo3Weapons)
            //    WriteScript(Scripts.Halo3Weapons);

            //if (TestAll || Defaults.AIO)
            //    WriteScript(Scripts.AIO);

            if (!Defaults.CloseOnExit)
                Console.ReadLine();
        }

        static void WriteScript(Scripts scriptsVersion)
        {
            using (Helpers h = new Helpers())
            {
                string ScriptName = null;

                h.Script.Add($@"UseAudioCache {Quote($@"{Defaults.AudioCachePath}\")}");
                h.Script.Add($"y");
                h.Script.Add("");
                switch (scriptsVersion)
                {
                    case Scripts.Halo3Multiplayer:
                        ScriptName = "Halo3MP";
                        h.CheckFile(ScriptName);
                        h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3Multiplayer, Defaults.Halo3MapsPath));
                        h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3MultiplayerDLC, Defaults.Halo3MapsPath));

                        //h.ScriptWriter(ForgePalette.MakePortCommands(GameVersion.Halo3Multiplayer));
                        //h.ScriptWriter(ForgePalette.MakePortCommands(GameVersion.Halo3MultiplayerDLC));

                        //h.ScriptWriter(PrematchCameraPoints.MakePortCommands(GameVersion.Halo3Multiplayer), Defaults.PrematchCameraPoints);
                        //h.ScriptWriter(PrematchCameraPoints.MakePortCommands(GameVersion.Halo3MultiplayerDLC), Defaults.PrematchCameraPoints);

                        //h.ScriptWriter(Fixes.Commands.Single());
                        //h.ScriptWriter(Fixes.Commands.Mass());
                        break;
                    case Scripts.Halo3Campaign:
                        ScriptName = "Halo3C";
                        h.CheckFile(ScriptName);
                        h.Script.AddRange(CampaignTips);
                        h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3Campaign, Defaults.Halo3MapsPath));
                        h.ScriptWriter(MetagameProperties.Commands());
                        break;
                    case Scripts.Halo3ODSTCampaign:
                        ScriptName = "Halo3ODST";
                        h.CheckFile(ScriptName);
                        h.Script.AddRange(CampaignTips);
                        h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3ODSTCampaign, Defaults.ODSTMapsPath));
                        h.ScriptWriter(MetagameProperties.Commands());
                        h.ScriptWriter(PlayerRepresentation.MakePortCommands(GameVersion.Halo3ODSTCampaign, Defaults.ODSTMapsPath));
                        break;
                    case Scripts.Halo3Hud:
                        ScriptName = "Halo3Hud";
                        h.CheckFile(ScriptName);
                        h.ScriptWriter(Hud.Commands());
                        break;
                    case Scripts.Halo3Weapons:
                        ScriptName = "Halo3Weapons";
                        h.CheckFile(ScriptName);
                        h.ScriptWriter(Weapons.Commands());
                        break;
                    case Scripts.AIO:
                        ScriptName = "All";
                        h.CheckFile(ScriptName);

                        if (TestAll || Defaults.Halo3MP)
                        {
                            h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3Multiplayer, Defaults.Halo3MapsPath));

                            h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3MultiplayerDLC, Defaults.Halo3MapsPath));

                            h.ScriptWriter(ForgePalette.MakePortCommands(GameVersion.Halo3Multiplayer));
                            h.ScriptWriter(ForgePalette.MakePortCommands(GameVersion.Halo3MultiplayerDLC));

                            h.ScriptWriter(PrematchCameraPoints.MakePortCommands(GameVersion.Halo3Multiplayer), Defaults.PrematchCameraPoints);
                            h.ScriptWriter(PrematchCameraPoints.MakePortCommands(GameVersion.Halo3MultiplayerDLC), Defaults.PrematchCameraPoints);

                            h.ScriptWriter(Fixes.Commands.Single());
                            h.ScriptWriter(Fixes.Commands.Mass());
                        }

                        if (TestAll || Defaults.Halo3Hud)
                            h.ScriptWriter(Hud.Commands());

                        if (TestAll || Defaults.Halo3Weapons)
                            h.ScriptWriter(Weapons.Commands());

                        if (TestAll || Defaults.Halo3C || Defaults.Halo3ODST)
                            h.Script.AddRange(CampaignTips);

                        if (TestAll || Defaults.Halo3C)
                            h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3Campaign, Defaults.Halo3MapsPath));

                        if (TestAll || Defaults.Halo3ODST)
                            h.ScriptWriter(Map.MakePortCommands(GameVersion.Halo3ODSTCampaign, Defaults.ODSTMapsPath));

                        if (TestAll || Defaults.Halo3C || Defaults.Halo3ODST)
                            h.ScriptWriter(MetagameProperties.Commands());
                        break;
                }
                File.WriteAllLines($@"{Defaults.CommandsOutputPath}\{ScriptName}.cmds", h.Script);
            }
        }
        public static string Quote(string input) => $"\"{input}\"";

        static readonly List<string> CampaignTips = new List<string> {
            "Echo Tips:",
            "Echo Use a mainmenu only cache when porting campaign (recommended for less problems, note this is not a requirement).",
            "Echo Don't forget -cache-memory-increase 1200 (900 is also a nice whole number too, note this is a requirement).",
            "Echo"
        };

        enum Scripts
        {
            Halo3Multiplayer,
            Halo3Campaign,
            Halo3ODSTCampaign,
            Halo3Hud,
            Halo3Weapons,

            AIO
        }
    }
}
